		
		
		
			1.关机重启命令
			
			shutdown
			
				shutdown -h now :表示立即关机
				shutdown -h 1 :表示一分钟后关机
				shutdown -now :立即重启
			
			halt
			
				直接使用，效果等价于关机
				
			reboot
			
				重启系统
				
			sync
			
				关机之前，把没保存的数据同步到磁盘上
				
			当我们关机或者重启服务器时，应该先执行sync指令，把内存的数据写入磁盘，防止数据丢失
			
			
			
			
			
			